<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $departments=["Marketing and Sales"," Human Resource","Finance","Operations","General Management"];
       
       foreach( $departments as $dep){
       Department::create(['name'=>$dep]);
       }
       
    }
}
