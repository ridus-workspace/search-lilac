<?php

use Illuminate\Database\Seeder;
use App\Models\Designation;

class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designation=["System Analyst", "Programmer Analyst", "Software Engineer",  "Senior Software Engineer", "Project Lead",
        "Sales Head",  "HR",  "Program Manager", "Deliver Manager","Director", "Vice President", "President","CEO","Finance Manager"];
       
        foreach( $designation as $des){
            Designation::create(['name'=>$des]);
        }
    }
}
