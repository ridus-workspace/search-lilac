<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        User::create(['name' => 'William', 'department_id' => 4, 'designation_id' => 1, 'phone_number' => 1]);
        User::create(['name' => 'Noah', 'department_id' => 2, 'designation_id' => 7, 'phone_number' => 1]);
        User::create(['name' => 'Liam', 'department_id' => 3, 'designation_id' => 14, 'phone_number' => 1]);
        User::create(['name' => 'Ubaid', 'department_id' => 5, 'designation_id' => 13, 'phone_number' => 1]);
        User::create(['name' => 'Anwar', 'department_id' => 4, 'designation_id' => 5, 'phone_number' => 1]);
        User::create(['name' => 'Rohan', 'department_id' => 4, 'designation_id' => 4, 'phone_number' => 1]);
        User::create(['name' => 'shabeer', 'department_id' => 4, 'designation_id' => 8, 'phone_number' => 1]);
        User::create(['name' => 'Harish', 'department_id' => 4, 'designation_id' => 2, 'phone_number' => 1]);
        User::create(['name' => 'Mukesh', 'department_id' => 5, 'designation_id' => 10, 'phone_number' => 1]);
        User::create(['name' => 'Sreya', 'department_id' => 5, 'designation_id' => 11, 'phone_number' => 1]);
      
        
    }
}
