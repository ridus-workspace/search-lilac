<?php

namespace App\Http\Services;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Http\Request;

class SearchService extends Controller
{
    /**
     * fuction to access method without object
     *
     * @return \Illuminate\Http\Response
     */
    private static $instance = null;

    public static function  getInstance()
    {
      
        if (self::$instance == null) {
            return $instance = new SearchService();
        }
        return self::$instance;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      
        return view('search.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search($request){

        $search=$request->search;
      
        $data=User::leftJoin('departments as dp','users.department_id','=','dp.id')
        ->leftJoin('designations as ds','users.designation_id','=','ds.id')
        ->select('users.name as name','dp.name as department','ds.name as designation');
        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                $query->where('users.name', 'LIKE', '%' . $search . '%')
                    ->orWhere('dp.name', 'LIKE', '%' . $search . '%')
                    ->orWhere('ds.name', 'LIKE', '%' . $search . '%');
            });}
        $data=$data->get();
        return response()->json(['status'=>1000,'data'=>$data]);
    }
}
