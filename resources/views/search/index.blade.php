<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <style>
        body {
            background: #e2e2e2;
            margin: 30px 0;
            padding: 0;
        }
        
        .box {
            background: #fff;
            padding: 15px;
            margin-bottom: 15px;
        }
        .arrange {
            float: right;
            margin-right: 2%;
        }


        
    </style>
</head>

<body>
    <section>
        <div class="breadcrumbs">
           
                <a class="arrange" href="">Home</a>
         
        </div>
       
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Search</h4>
                    <div class="form-group">
                        <input type="text" id="search"class="form-control search" autocomplete="off" placeholder="Search /name/designation/department">
                    </div>
                </div>
            </div>
            <div class="row search-list-class">
                
            </div>
        </div>
    </section>
    @include('search.js.search')
  </body> 
 
</html>