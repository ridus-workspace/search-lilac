<script type="text/javascript" src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>


<script>
    $(document).ready(function () {
     $("#search").trigger("keyup");
    });

    $(document).on('keyup', '#search', function () {
        var search = $('#search').val();

        $.ajax({
            url: "{{route('search.list')}}",
            method: 'get',
            data: {
                'search': search
            },

            success: function (res) {

                if (res.status == 1000) {
                    var html = '';
                   
                    if (res.data.length > 0 && res.data != null) {
                        $.each(res.data, function (index, value) {

                            html += '<div class="col-md-6">  <div class="box"><h3>' + value
                                .name + '</h3> <h6>' + value.designation + '</h6>  <p>' +
                                value.department + '</p> </div> </div>';
                        });
                      
                    }
                    $('.search-list-class').empty().append(html);

                }
            },
            error: function (response) {


            },
        });
    })

</script>
